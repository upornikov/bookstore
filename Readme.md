# Django web app - Bookstore prototype to handle bookstore entries in a simulated library;

 - model webform interface for handling Author, Publisher, Book models:
 - REST API interface for the models;
 - Docker support for linux development environments;
 - Dockerized hosting of a local gitlab server;
 - Docker support for CI/CD with the local gitlab server;
 - Docker support for CI/CD with the remote gitlab server (https://gitlab.com);

# Requirements:
 - python 3.6;
 - django 1.11;
 - docker;
 - docker-compose;


# Start the app with local dev/live server in one of the following ways:

1. In the host dev.environment (localhost:8000) as:
 /> pip install -r requirements.txt
 /> python manage.py runserver 0.0.0.0:8000

2. Dev.setup on debian docker image (localhost:8000):
 /> cd ./docker/dev && docker-compose up -d dev-debian

3. Dev.setup on centos docker image (localhost:8001):
 /> cd ./docker/dev && docker-compose up -d dev-centos

4. Live setup (via nginx+gunicorn) on debian docker image (localhost:8081):
 /> cd ./docker/dev && docker-compose up -d live-debian


# Continuous Integration/Deployment is available for local and remote gitlab servers:

1. Run local gitlab:
 /> cd ./docker/ci && docker-compose up -d gitlab
 
2. Start gitlab runners for parallel unittesting triggered by local/remote gitlab servers:
 /> cd ./docker/ci && docker-compose up -d runner-ut
 /> cd ./docker/ci.gitlab.com && docker-compose up -d gitlab-runner-ut
 
3. Start gitlab runners for deployment triggered by local/remote gitlab servers:
 /> cd ./docker/ci && docker-compose up -d runner-deploy
 /> cd ./docker/ci.gitlab.com && docker-compose up -d gitlab-runner-deploy

4. When the gitlab runners are online:
 - unittests are triggered automatically with every new commit (or manually); 
 - deployment is configured to be started manually, if unittests are ok; 
