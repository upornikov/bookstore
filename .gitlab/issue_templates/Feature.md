Feature Summary

(Summarize the requested feature concisely)


Use cases

(User scenarios for the feature - this is very important)


Feature Demo Project

(Example project here on GitLab.com that demonstrates the feature details, and link to it here)


Relevant pics, diagrams and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)


Architecture/design limitations:
(Provide design limitations if any)

Possible design proposals:
(Outline possible desing solutions for the feature)


/label ~feature ~needs-design ~needs-implementation ~needs-review
/cc @project-manager
/assign @developer
