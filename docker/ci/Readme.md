# Gitlab on Docker

	[Install/config gitlab server on Docker](https://docs.gitlab.com/omnibus/docker/README.html)
	[gitlab on Docker container](https://docs.gitlab.com/omnibus/docker/README.html#run-the-image)
	[gitlab with docker-compose](https://docs.gitlab.com/omnibus/docker/README.html#install-gitlab-using-docker-compose)
	[gitlab on Docker cluster](https://docs.gitlab.com/omnibus/docker/README.html#install-gitlab-into-a-cluster)

# How it goes...

 - the contents of docker/ci/* provides CI/CD configuration for bookstore repo on local gitlab;
 - gitlab-ci.yml, when copied to project root, enables CI automation;
 - 'docker-compose up -d gitlab' to provivison gitlab server;
 - 'docker-compose up -d runner-ut' to provivison gitlab runner for parallel unittests;
 - 'docker-compose up -d runner-staging' to provivison gitlab runner for staging deployment;
 - with every new commit, gitlab feeds unittests to the unittest runner;
 - staging deployment is triggered from gitlab UI manually;
 - run_tests_async.sh is a shell script to run tests asynchronously as background processes in a single runner;
 
