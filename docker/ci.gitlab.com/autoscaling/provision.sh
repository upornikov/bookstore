#!/bin/sh

sudo apt-get update -y
sudo apt-get install -y curl git

sudo apt-get install -y software-properties-common
sudo curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get install apt-transport-https
sudo apt-get update -y
sudo apt-get install -y docker-ce

#install docker-machine for autoscaling with docker-machine


sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-linux-amd64 && \
sudo chmod +x /usr/local/bin/gitlab-runner

# Create new user for runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
 
sudo gitlab-runner unregister --name "gitlab.com scalable unittest runner"
sudo gitlab-runner register \
    	-u https://gitlab.com \
    	-r M5RxqAV_3VEm7x9A-EGC \
    	-n \
    	--executor docker \
    	--docker-image python \
    	--tag-list "python,ut" \
    	--name "gitlab.com scalable unittest runner"
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo sed -i -e "s/concurrent = 1/concurrent = 3/" /etc/gitlab-runner/config.toml
sudo gitlab-runner start

