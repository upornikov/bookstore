# Gitlab autoscaling CI/CD

	[Gitlab CI/CD autoscaling mode](https://docs.gitlab.com/runner/configuration/autoscale.html)
	[Run gitlab runner in autoscaling mode with docker-machine as executor](https://docs.gitlab.com/runner/install/autoscaling.html)

# How it goes...

 - 'vagrant up' starts local starts VirtualBox image by Vagrantfile configuration and provisions it to be run in autoscaling mode with docker as executor;
 - 'vagrant ssh' opens ssh console to the image; 
 - 'vagrant down' stops the image; 
 - 'vagrant destroy' destroys the image; 
 - !Autoscaling with docker-machine still to go:


