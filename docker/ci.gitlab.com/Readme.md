# Gitlab on Docker

	[Install/config gitlab server on Docker](https://docs.gitlab.com/omnibus/docker/README.html)
	[gitlab on Docker container](https://docs.gitlab.com/omnibus/docker/README.html#run-the-image)
	[gitlab with docker-compose](https://docs.gitlab.com/omnibus/docker/README.html#install-gitlab-using-docker-compose)
	[gitlab on Docker cluster](https://docs.gitlab.com/omnibus/docker/README.html#install-gitlab-into-a-cluster)

# How it goes...

 - this is a CI/CD configuration for remote https://gitlab.com/upornikov/bookstore repo;
 - gitlab-ci.yml enables automation, when copied to the project root;
 - the test host (e.g. VirtualBox image) must have docker and docker-compose installed;
 - 'docker-compose up -d gitlab-runner-ut' to provision private gitlab runner for parallel dockerized unittests run by the remote repo;
 - 'docker-compose up -d gitlab-runner-staging' to provision private gitlab runner to unittest and deploy the live app to its docker container; 
 - with every new commit, gitlab.com feeds unittests to the unittest runner;
 - staging deployment is triggered from gitlab UI manually;
 - run_tests_async.sh is a shell script to run tests asynchronously as background processes in a single runner;
 
