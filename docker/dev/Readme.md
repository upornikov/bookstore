# Requirements:
	- docker
	- docker-compose

# Create&run/destroy all services (run from the folder with docker-compose.yml)

    $ docker-compose up -d
    $ docker-compose down

## Remove all service together with all images:

    $ docker-compose down --rmi all

# Create&run/destroy src service

    $ docker-compose up -d <service_name>
    $ docker-compose down <service_name>

## After service is created, start/stop service containers by container names

    $ docker start <container_name>
    $ docker stop <container_name>
