#!/bin/sh
#
# The script runs test in parallel background processes.
# Overal result presents a sum of the exit codes.
#

(python ../manage.py test api) &
api_pid=$!
(python ../manage.py test app) &
app_pid=$!
echo Started: api_pid=$api_pid, app_pid=$app_pid

wait $api_pid
api_res=$?
wait $app_pid
app_res=$?
res=$((api_res + app_res))

echo Done with processes $api_pid $app_pid
echo api test result=$api_res
echo app test result=$app_res
echo overal result=$res

exit $res

