from django.contrib import admin

# Register your models here.
from app.models import Author, Book, Publisher

admin.site.register(Author)
admin.site.register(Book)
admin.site.register(Publisher)

