from django.conf.urls import url
from app import views

urlpatterns = [
        url(r'^$', views.index, name='app_index'),
		url(r'^add/$', views.add, name='add'),
		url(r'^add_author/$', views.add_author, name='add_author'),
		url(r'^add_publisher/$', views.add_publisher, name='add_publisher'),
		url(r'^add_book/$', views.add_book, name='add_book'),
]
