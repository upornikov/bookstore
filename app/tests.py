from django.utils import timezone
from django.test import TestCase
from app.models import Author, Book, Publisher

class AuthorTest(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass
         
    def test_update_author(self):
        # Same author may have various CapITaliZatIOn, or word order
        names = ['The name', 'THE NAME', 'naME thE']
        name0 = names[0]
        for name in names:
            #save updates Author fields 
            Author(name).save() 
            self.assertEqual(Author.objects.count(), 1)
            self.assertEqual(Author.objects.all()[0].name, name)
        another_name = 'another name'
        Author(another_name).save()
        self.assertEqual(Author.objects.count(), 2)
        self.assertTrue(Author.objects.filter(name=another_name).exists())
 
    def test_get_or_create_author(self):
        # Same author can be requested by various name spelling
        names = ['The name', 'THE NAME', 'naME thE']
        name0 = names[0]
        for name in names:
            Author(name).get_or_create() # first call creates, other just get
            self.assertEqual(Author.objects.count(), 1)
            self.assertEqual(Author.objects.all()[0].name, name0)
        
        name2 = names[2]
        # This call will replace name spelling for the Author
        Author.objects.all()[0].update(name=names[2], dob='2000-01-01')
        self.assertEqual(Author.objects.count(), 1)
        self.assertEqual(Author.objects.all()[0].name, name2)
        self.assertEqual(Author.objects.all()[0].dob, 
                         timezone.datetime.strptime('2000-01-01', '%Y-%m-%d').date())
        
        another_name = 'another name'
        # Another author will be created
        Author(another_name).get_or_create()
        self.assertEqual(Author.objects.count(), 2)
        self.assertTrue(Author.objects.all().filter(name=another_name).exists())
        

class BookTest(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass
         
    def test_create_book_add_remove_author(self):
        self.assertEqual(Author.objects.count(), 0)
        self.assertEqual(Book.objects.count(), 0)
        self.assertEqual(Publisher.objects.count(), 0)
        names = ['The name', 'THE NAME', 'naME thE'] # diff.spellings - same author
        name0 = names[0]
        title = 'the book'
        Book(title).save()
        self.assertEqual(Book.objects.count(), 1)
        b = Book.objects.all()[0]
        self.assertEqual(b.title, title)
        for name in names:
            b.add_author(name)  #get or create author
        self.assertEqual(Author.objects.all().count(), 1)
        self.assertEqual(b.authors.all().count(), 1)
        self.assertEqual(b.authors.all()[0].name, name0)
        b.remove_author(names[-1])  # name spelling may differ
        self.assertEqual(b.authors.all().count(), 0)
        self.assertEqual(Author.objects.all().count(), 1)
         
    def test_create_book_add_publisher(self):
        b = Book('the book')
        b.save()
        self.assertEqual(Book.objects.all().count(), 1)
        title1 = 'publisher 1'
        Publisher(title1).save()
        b.set_publish_data(title1, '2000-01-01')
        self.assertEqual(Book.objects.all()[0].publisher, Publisher.objects.all()[0])
        self.assertEqual(Book.objects.all()[0].date, timezone.datetime.strptime('2000-01-01', '%Y-%m-%d').date())


class FormsTest(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    #TODO        
        