from django.contrib.auth.models import User
from rest_framework import serializers
from app.models import Author, Book, Publisher


class PublisherSerializer(serializers.HyperlinkedModelSerializer):
    book_set = serializers.HyperlinkedRelatedField(many=True, view_name='book-detail', read_only=True)
    class Meta:
        model = Publisher
        fields = ('url', 'publisher_id', 'title', 'book_set')


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Author
        fields = ('url', 'author_id', 'name', 'dob', 'tag', 'books')


class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Book
        fields = ('url', 'book_id', 'title', 'publisher', 'date', 'authors', 'librarian')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    library_entries = serializers.HyperlinkedRelatedField(many=True, view_name='book-detail', read_only=True)
    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'library_entries')
    