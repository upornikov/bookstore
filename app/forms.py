from django import forms
from app.models import Author, Book, Publisher
from datetime import datetime

class PublisherForm(forms.ModelForm):
    title = forms.CharField(max_length=255, help_text='Title')
    class Meta:
        model = Publisher
        fields = ('title', )

class AuthorForm(forms.ModelForm):
    name = forms.CharField(max_length=255, help_text='Name')
    dob = forms.DateField(widget=forms.SelectDateWidget(years=range(1900,datetime.now().year)), 
                          help_text='Date of birth')
#    tag = forms.CharField(widget=forms.HiddenInput(), required=False)
    class Meta:
        model = Author
        fields = ('name', 'dob',)
#    def __init__(self, *args, **kwargs):
#        super(AuthorForm, self).__init__(*args, **kwargs)
#        if 'update_author' in args[0]:
#            self.fields['update'].initial = True
#    def save(self, commit=True, update=False):
#        self.cleaned_data['tag'] = normalize_name(self.cleaned_data['name'])
#        print("Saving Author:", self.cleaned_data)
#        group = super(AuthorForm, self).save(commit=commit)
#        return group


class BookForm(forms.ModelForm):
    title = forms.CharField(max_length=255, help_text='Title')
    authors = forms.ModelMultipleChoiceField(help_text='Authors', queryset=Author.objects.all())
    publisher = forms.ModelChoiceField(help_text='Publisher', queryset=Publisher.objects.all())
    date = forms.DateField(widget=forms.SelectDateWidget(years=range(1900,datetime.now().year+1)),
                           help_text='Date of issue')
    class Meta:
        model = Book
        fields = ('title', 'authors', 'publisher', 'date')
	