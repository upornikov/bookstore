from django.utils import timezone
from django.db import models
#from django.dispatch import receiver
from django.db.models.signals import post_init
from django.http import Http404 
        

class Publisher(models.Model):
    title = models.CharField(max_length=255, blank=False, verbose_name='Publisher title')
    publisher_id = models.AutoField(primary_key=True)
    def __str__(self):
        return self.title
    def save(self, *args, **kwargs):
        try:
            p = Publisher.objects.get(title=self.title)
            self.pk = p.pk
            p.refresh_from_db()
        except Publisher.DoesNotExist:
            super(Publisher, self).save(*args, **kwargs)
    class Meta:
        ordering = ('title', )

def normalize_name(name):
    n = name.lower().split()
    n.sort()
    return ' '.join(n)
       
class Author(models.Model):
    name = models.CharField(max_length=255, blank=False, verbose_name='Author name', primary_key=False)
    dob = models.DateField(blank=True, null=False, default=timezone.localdate)
    tag = models.CharField(max_length=255, blank=True, null=False)
#    books is Book m2m related field
    author_id = models.AutoField(primary_key=True)
    def __str__(self):              # __unicode__ on Python 2
        return self.name
    def get_or_create(self, *args, **kwargs):
        if self.name and not self.tag:
            self.tag = normalize_name(self.name)
        try:
            existing = Author.objects.get(tag=self.tag)
            self.pk = existing.pk
            self.refresh_from_db()  # keep existing values 
        except Author.DoesNotExist:
            super(Author, self).save(*args, **kwargs)
        return self
    def update(self, *args, **kwargs):
        self.name = kwargs.get('name') or self.name
        self.dob = kwargs.get('dob') or self.dob
        if self.name and not self.tag:
            self.tag = normalize_name(self.name)
        if self.tag != normalize_name(self.name):
            raise Http404("Invalid new name: " + self.name)
        try:
            old = Author.objects.get(tag=self.tag)
            #update specified fields
            old.name = self.name
            old.dob = self.dob or old.dob
            super(Author, old).save()
            self.pk = old.pk
            self.refresh_from_db() 
        except Author.DoesNotExist:
            super(Author, self).save(*args, **kwargs)
        return self
    def save(self, *args, **kwargs):
        self.update(*args, **kwargs)
    class Meta:
        ordering = ('name', 'dob',)

#@receiver(post_init, sender=Author)
def create_author_tag(sender, **kwargs):
    author = kwargs.get('instance')
    if not author or not author.name:
        return
    if author.name and not author.tag:
        author.tag = normalize_name(author.name)
post_init.connect(create_author_tag, sender=Author)


class Book(models.Model):
    title = models.CharField(max_length=255, blank=False, verbose_name='Book title')
    publisher = models.ForeignKey('Publisher', on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateField(null=False, default=timezone.localdate)
    authors = models.ManyToManyField('Author', related_name='books')
    librarian = models.ForeignKey('auth.User', related_name='library_entries', on_delete=models.CASCADE, blank=True, null=True)
    book_id = models.AutoField(primary_key=True)
    def __str__(self):
        return self.title
    def add_author(self, name):
        """
        name may contain several words: capitalization and word sequence don't matter.
        If author exists (search by author tag), then add this author,
        else create new author and add.
        """
        if not self.authors.all().filter(tag=normalize_name(name)).exists():
            self.authors.add(Author(name).get_or_create())
#        else:
#            print("add_author:", name, "already there")
    def remove_author(self, name):
        try:
            author = self.authors.all().get(tag=normalize_name(name))
            self.authors.remove(author)
        except app.models.DoesNotExist:
            raise Http404("remove_author: " + name + " not found")            
    def dump_authors_names(self):
        names = [a.name for a in self.authors.all()]
        return ', '.join(names)
    def set_publish_data(self, publisher_title, publish_date=timezone.now):
        try:
            p = Publisher.objects.all().get(title=publisher_title)
            self.publisher = p
            self.date = publish_date
            self.save()
        except Publisher.DoesNotExist:
            raise Http404("set_publish_data: " + publisher_title + " not found")            
    class Meta:
        ordering = ('title',)
       

   
    