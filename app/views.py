from django.shortcuts import render, redirect
from app.forms import AuthorForm, BookForm, PublisherForm
from app.models import Book

# Create your views here.
from django.http import HttpResponse
def index(request):
#    return HttpResponse("bookstore.app is here!")
   # HTTP GET only
    if not request or request.method == 'GET':
        # display the form to enter details.
        order_by = request.GET.get('order_by', 'book_id')
        book_list = Book.objects.all().order_by(order_by)
    return render(request, 'app/books.html', {'book_list': book_list})

def add_author(request):
    # mainly for HTTP POST; GET should be processed by add()
    if request.method == 'POST':
        form = AuthorForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return redirect('/add/')
        else:
            print(form.errors)
    else:
        form = AuthorForm()
    return render(request, 'app/add.html', {'author_form': form})

def add_publisher(request):
    # mainly for HTTP POST; GET should be processed by add()
    if request.method == 'POST':
        form = PublisherForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return redirect('/add/')
        else:
            print(form.errors)
    else:
        form = PublisherForm()
    return render(request, 'app/add.html', {'publisher_form': form})

def add_book(request):
    # mainly for HTTP POST; GET should be processed by add()
    if request.method == 'POST':
        form = BookForm(request.POST)
        if form.is_valid():
            book_mod = form.save(commit=True)
            return redirect('/')
        else:
            print(form.errors)
    else:
        form = BookForm()
    return render(request, 'app/add.html', {'book_form': form})

def add(request=None):
    # HTTP GET only
    if not request or request.method == 'GET':
        # display the form to enter details.
        author_form = AuthorForm()
        publisher_form = PublisherForm()
        book_form = BookForm()
    return render(request, 'app/add.html', 
                           {'author_form': author_form, 
                            'publisher_form': publisher_form, 
                            'book_form': book_form, 
                           })
