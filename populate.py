import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bookstore.settings')

import django
django.setup()

from app.models import Author, Book, Publisher


names = ['a 1', 'a 2', 'a 3', 'a 4', 'a 5']
book_titles = ['b 1', 'b 2', 'b 3', 'b 4', 'b 5', 'b 6', 'b 7', 'b 8', 'b 9', ]
pub_titles = ['p 1', 'p 2', 'p 3', 'p 4']

for n in names:
    Author(n).save()
for t in book_titles:
    Book(t).save()
for t in pub_titles:
    Publisher(t).save()
    
p_ind = 0
p_count = Publisher.objects.count()
a_ind = 0
a_count = Author.objects.count()
for b in Book.objects.all():
    b.publisher = Publisher.objects.all()[p_ind%p_count]
    b.save()
    p_ind += 1
    author_list = list(Author.objects.all()[a_ind%a_count:])
    b.authors.add(*author_list)
    a_ind += 1

print('Populated')
