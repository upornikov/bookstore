from django.shortcuts import render

from django.http import HttpResponse
from django.contrib.auth.models import User
from app.models import Author, Book, Publisher
from app.serializers import AuthorSerializer, BookSerializer, PublisherSerializer, UserSerializer
from rest_framework import generics, permissions

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


#def index(request):
#    return HttpResponse("Hi there from API!")

@api_view(['GET'])
def index(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'authors': reverse('author-list', request=request, format=format),
        'publishers': reverse('publisher-list', request=request, format=format),
        'books': reverse('book-list', request=request, format=format)
    })


class AuthorList(generics.ListCreateAPIView):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly,)

class AuthorDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly,)


class BookList(generics.ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
#    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly,)
    def perform_create(self, serializer):
        serializer.save(librarian=self.request.user)

class BookDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
#    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly,)
    def perform_update(self, serializer):
        serializer.save(librarian=self.request.user)


class PublisherList(generics.ListCreateAPIView):
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly,)

class PublisherDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly,)


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
