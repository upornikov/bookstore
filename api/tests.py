import json
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.reverse import reverse
from app.models import Author, Book, Publisher


def populate():
    names = ['a 0', 'a 1', 'a 2', 'a 3', 'a 4']
    book_titles = ['b 0', 'b 1', 'b 2', 'b 3', 'b 4', 'b 5', 'b 6', 'b 7', 'b 8', 'b 9', ]
    pub_titles = ['p 0', 'p 1', 'p 2', 'p 3', 'p 4']
    for a in names:
        Author(a).save()
    for b in book_titles:
        Book(b).save()
    for p in pub_titles:
        Publisher(p).save()
    p_ind = 0
    p_count = Publisher.objects.count()
    a_ind = 0
    a_count = Author.objects.count()
    for b in Book.objects.all():
        b.publisher = Publisher.objects.all()[p_ind%p_count]
        b.save()
        p_ind += 1
        author_list = list(Author.objects.all()[a_ind%a_count:])
        b.authors.add(*author_list)
        a_ind += 1


class TestGetModelApi(TestCase):
    def setUp(self):
        self.client = APIClient()
        populate()

    def test_get_Book_list(self):
        response = self.client.get(reverse('book-list'))
        res = json.loads(response._container[0].decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(res['count'], Book.objects.count())
        #todo: check book instances from res['results'] list?

    def test_get_Book_detail(self):
        book = Book.objects.all()[0]
        response = self.client.get(reverse('book-detail', args=[book.book_id]))
        res = json.loads(response._container[0].decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(res['book_id'], book.book_id)
        self.assertEqual(res['title'], book.title)

    def test_get_Author_list(self):
        response = self.client.get(reverse('author-list'))
        res = json.loads(response._container[0].decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(res['count'], Author.objects.count())

    def test_get_Author_detail(self):
        author = Author.objects.all()[0]
        response = self.client.get(reverse('author-detail', args=[author.author_id]))
        res = json.loads(response._container[0].decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(res['author_id'], author.author_id)
        self.assertEqual(res['name'], author.name)

    def test_get_Publisher_list(self):
        response = self.client.get(reverse('publisher-list'))
        res = json.loads(response._container[0].decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(res['count'], Publisher.objects.count())

    def test_get_Publisher_detail(self):
        publisher = Publisher.objects.all()[0]
        response = self.client.get(reverse('publisher-detail', args=[publisher.publisher_id]))
        res = json.loads(response._container[0].decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(res['publisher_id'], publisher.publisher_id)
        self.assertEqual(res['title'], publisher.title)

    #TODO: test responce details/hyperlinks?
    #TODO: test permissions?
    

class TestPutModelApi(TestCase):
    #TODO: implementaion
    pass
